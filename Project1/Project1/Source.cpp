#include <iostream>
#include <ctime>
#include <iomanip>
#define RANDOM (rand() % MAXBORDER - MINBORDER)
using namespace std;
const int N = 17;
const int MAXBORDER = 25;
const int MINBORDER = 12;
enum Menu
{
	EXIT = 0,   
	READ,
	LOW, 
	NUL, 
	SUM,
	NEW, 
	DELETE
};

void fillMatrix(int(&matrix)[N][N]);
void OutputMatrix(int(&matrix)[N][N]);
int ElementsSum(int(&matrix)[N][N]);
int NotZeroLines(int(&matrix)[N][N]);
void BelowCount(int(&matrix)[N][N]);

int main() {
	
	srand(time(0));
	setlocale(LC_ALL, "Russian");
	int matrix[N][N];


	
	fillMatrix(matrix);
	int exit=1;
	while (exit)
	{
		int var = 0;
	
		cout << "����:" << endl;
		cout << "0. ����� " << endl;
		cout << "1. ������� ���������� ������� �� �����" << endl;
		cout << "2. ���-�� ����� ������ ��������� �����" << endl;
		cout << "3. ���������� ��������� �����" << endl;
		cout << "4. ����� ����� ����� �������� ���������" << endl;
		cout << "5. �������� ����� �������" << endl;
		cout << "6. �������� ���� �����" << endl;
			cin >> var;
			
				switch (var) {
				case EXIT:
					exit = 0;
					break;
				


				case READ:
					OutputMatrix(matrix);
					break;


				case LOW:
					cout << "���-�� ����� ������ ��������� �����:" << endl;
					BelowCount(matrix);
					break;



				case NUL:
					cout << "���������� ��������� �����:" << NotZeroLines(matrix) << endl;
					break;


				case SUM:
					cout << "����� ����� ����� �������� ���������:" << ElementsSum(matrix) << endl;
					break;
				case NEW:
					fillMatrix(matrix);
					cout << "������� �������" << endl;
					break;
				case DELETE:
					system("cls");
					break;
					
				default:
					std::cout << "������� ����� �� 0 �� 6" << endl;
				}

	}


	system("pause");

}


void fillMatrix(int(&matrix)[N][N]) {

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			matrix[i][j] =RANDOM;
		}
	}
}


void OutputMatrix(int(&matrix)[N][N]) {
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			cout << setw(5) << matrix[i][j];
		}
		cout << endl;
	}
	cout << endl << endl;
}


int ElementsSum(int(&matrix)[N][N]) { //�����
	int sum = 0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < i; j++) {
			sum += matrix[i][j];
		}
	}
	return sum;
}


int NotZeroLines(int(&matrix)[N][N]) { //��������� �� ������� ������
	int sum = 0;
	bool check;
	for (int i = 0; i < N; i++) {
		check = false;
		for (int j = 0; j < N; j++) {
			if (matrix[i][j] != 0) {
				check = true;
				break;
			}
		}
		if (check) {
			sum++;
		}
	}
	return sum;
}


void BelowCount(int(&matrix)[N][N]) {
	int a;
	cout << "������� �����, ���� �������� ���������� ����� ���������� ���������:"; cin >> a;
	int b[N] = {0};
	for (int i = 0; i < N; i++) {
		
		for (int y = 0; y < N; y++) {
			if (a > matrix[i][y]) {
				b[i]++;
			}

		}

	}
	for (int i = 0; i < N; i++) {
		cout << "� " << i + 1 << " ������:" << b[i] << endl;
	}
}

//comit